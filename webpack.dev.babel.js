import HtmlPlugin from 'html-webpack-plugin';
import merge from 'webpack-merge';
import path from 'path';
import { HotModuleReplacementPlugin, NamedModulesPlugin } from 'webpack';
import WriteFilePlugin from 'write-file-webpack-plugin';
import CleanPlugin from 'clean-webpack-plugin';
import common from './webpack.common.babel';

process.env.BABEL_ENV = 'web';

const PLUGIN_PATH = 'C:/Program Files/Adobe/Adobe Photoshop CC 2017/Required/CEP/extensions/org.isbdnt.ps2blender';

const PORT = 3000;

export default merge(common, {
  entry: {
    main: [
      'react-hot-loader/patch',
      // activate HMR for React

      `webpack-dev-server/client?http://localhost:${PORT}`,
      // bundle the client for webpack-dev-server
      // and connect to the provided endpoint

      'webpack/hot/only-dev-server',
      // bundle the client for hot reloading
      // only- means to only hot reload for successful updates

      './js/main.js',
      // the entry point of our app
    ],
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              outputStyle: 'expanded',
              includePaths: [path.join(__dirname, 'src/main/resources/scss')],
            },
          },
        ],
      },
    ],
  },
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, PLUGIN_PATH),
  },
  devtool: '#cheap-module-eval-source-map',
  devServer: {
    hot: true,
    port: PORT,
    publicPath: '/',
    stats: 'minimal',
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'out/dev'),
  },
  plugins: [
    new HotModuleReplacementPlugin(),
    new NamedModulesPlugin(),
    new HtmlPlugin({
      template: './resources/html/index.html',
      filename: 'index.html',
    }),
    new WriteFilePlugin(),
    new CleanPlugin(['out']),
  ],
});
