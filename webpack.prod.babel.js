import HtmlPlugin from 'html-webpack-plugin';
import merge from 'webpack-merge';
import path from 'path';
import CleanPlugin from 'clean-webpack-plugin';

import common from './webpack.common.babel';

export default merge(common, {
  entry: {
    main: './js/main.js',
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              includePaths: [path.join(__dirname, 'src/main/resources/scss')],
            },
          },
        ],
      },
    ],
  },
  // devtool: 'source-map',
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, 'out/prod/org.isbdnt.ps2blender'),
  },
  devServer: {
    port: 5000,
    stats: 'normal',
    historyApiFallback: true,
  },
  plugins: [
    new HtmlPlugin({
      template: './resources/html/index.html',
      filename: 'index.html',
    }),
    new CleanPlugin(['out']),
  ],
});
