import { Component } from 'react';
import List, { ListItem } from 'material-ui/List';
import { connect } from 'react-redux';
import h from 'react-hyperscript';
import Button from 'material-ui/Button';
import { clickImage, removeImage } from 'controllers';

class Images extends Component {
  render = () => {
    const { images } = this.props;
    return h('div', {
      children: [
        h(List, {
          dense: true,
          children: images.map(({ id, name, isLinking }) => h(ListItem, {
            style: {
              padding: '3px 12px',
            },
            children: [
              h(Button, {
                style: {
                  textTransform: 'none',
                  width: '100%',
                  whiteSpace: 'nowrap',
                  color: isLinking ? '#fff' : '#000',
                  fontSize: 12,
                },
                children: name,
                raised: true,
                color: isLinking ? 'accent' : 'contrast',
                onClick: clickImage.bind(null, id, isLinking),
              }),
              h(Button, {
                color: 'accent',
                raised: true,
                children: 'x',
                style: {
                  minWidth: 35,
                  marginLeft: 5,
                  fontSize: 12,
                },
                onClick: removeImage.bind(null, id, name),
              }),
            ],
          })),
        }),
      ],
    });
  };
}

const mapState = ({ documents, activeDocumentName }) => ({
  images: activeDocumentName ? documents[activeDocumentName] : [],
});
const mapDispatch = () => ({});
export default connect(mapState, mapDispatch)(Images);
