import { Component } from 'react';
import { linkImage, connectBlender } from 'controllers';
import Button from 'material-ui/Button';
import h from 'react-hyperscript';

export default class ToolBar extends Component {
  render = () => h('div', {
    style: {
      display: 'flex',
      justifyContent: 'space-around',
    },
    children: [
      h(Button, {
        onClick: linkImage,
        raised: true,
        color: 'primary',
        children: 'Link Image',
        style: {
          textTransform: 'none',
        },
      }),
      h(Button, {
        raised: true,
        color: 'primary',
        onClick: connectBlender,
        children: 'Connect Blender',
        style: {
          textTransform: 'none ',
        },
      }),
    ],
  });
}
