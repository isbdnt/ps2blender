import { Component } from 'react';
import Paper from 'material-ui/Paper';
import List, { ListItem } from 'material-ui/List';
import { test } from 'eval-ps-scripts';
import Button from 'material-ui/Button';
import h from 'react-hyperscript';
import { connect } from 'react-redux';
import { production } from 'config';

class Console extends Component {
  _count = 0;

  scrollToBottom = () => {
    requestAnimationFrame(() => {
      this.refs.list.scrollTop = this.refs.list.scrollHeight;
    });
  };

  render = () => {
    if (production) {
      return null;
    }

    const { messages } = this.props;
    if (this._count < messages.length && this.refs.list) {
      this.scrollToBottom();
    }
    return h(Paper, {
      children: [
        h('div', {
          ref: 'list',
          style: {
            height: 200,
            overflowY: 'auto',
          },
          children: h(List, {
            dense: true,
            children: messages.map((msg, key) => h(ListItem, { children: msg, key })),
          }),
        }),
        h(Button, {
          raised: true,
          color: 'accent',
          children: 'test',
          onClick: async () => {
            const result = await test();
            debug(result);
          },
        }),
      ],
    });
  };
}

const mapState = ({ stdoutMessages }) => ({ messages: stdoutMessages });
const mapDispatch = () => ({});

export default connect(mapState, mapDispatch)(Console);
