import { addStdoutMessage } from 'actions';
import { dispatch } from 'store';
import { TEMP_PATH,} from 'config';
import {fs, path} from 'nodejs';

// Promise = bb;

const log = console.log.bind(console);

console.log = (message) => {
  dispatch(addStdoutMessage((message.toString())));
  log(message);
};

window.onerror = (err) => {
  console.log(err);
};
window.evalPs = (script, ...params) => {
  return new Promise((resolve, reject) => {
    return window.__adobe_cep__.evalScript(`(${script.toString()})(${params.join(',')})`, (result) => {
      resolve(result)
    });
  })
};
window.debug = console.log;

window.onunhandledrejection = function (event) {
  console.log(event);
};
process.on('uncaughtException', (err) => {
  console.log(err);
});
try {
  fs.mkdir(TEMP_PATH)
} catch (e) {

}

