import { ADD_STDOUT_MESSAGE } from 'action-types';

export default message => ({
  type: ADD_STDOUT_MESSAGE,
  message,
});
