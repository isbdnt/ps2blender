import { ADD_IMAGE } from 'action-types';


export default (documentName, imageId, imageName) => ({
  type: ADD_IMAGE,
  documentName,
  imageName,
  imageId,
});
