import addStdoutMessage from './addStdoutMessage';
import addDocument from './addDocument';
import addImage from './addImage';
import setImageLinking from './setImageLinking';
import setActiveDocumentName from './setActiveDocumentName';
import removeImage from './removeImage';
import setImageName from './setImageName';
import setConnectingBlender from './setConnectingBlender';
import setAllImageLinking from './setAllImageLinking';

export {
  addStdoutMessage, addDocument, addImage, setImageLinking, setActiveDocumentName,
  removeImage, setImageName, setConnectingBlender, setAllImageLinking,
};
