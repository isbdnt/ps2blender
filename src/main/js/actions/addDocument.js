import { ADD_DOCUMENT } from 'action-types';

export default documentName => ({
  type: ADD_DOCUMENT,
  documentName,
});
