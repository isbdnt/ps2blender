import { SET_CONNECTING_BLENDER } from 'action-types';

export default isConnecting => ({
  type: SET_CONNECTING_BLENDER,
  isConnecting,
});
