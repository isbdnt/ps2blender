import { SET_IMAGE_NAME } from 'action-types';

export default (documentName, imageId, imageName) => ({
  type: SET_IMAGE_NAME,
  documentName,
  imageId,
  imageName,
});
