import { SET_ALL_IMAGE_LINKING } from 'action-types';

export default (documentName, isLinking) => ({
  type: SET_ALL_IMAGE_LINKING,
  documentName,
  isLinking,
});
