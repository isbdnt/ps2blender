import { SET_IMAGE_LINKING } from 'action-types';


export default (documentName, imageId, isLinking) => ({
  type: SET_IMAGE_LINKING,
  documentName,
  imageId,
  isLinking,
});
