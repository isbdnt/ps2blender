import { REMOVE_IMAGE } from 'action-types';

export default (documentName, imageId) => ({
  type: REMOVE_IMAGE,
  documentName,
  imageId,
});
