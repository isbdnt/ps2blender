import { SET_ACTIVE_DOCUMENT } from 'action-types';


export default documentName => ({
  type: SET_ACTIVE_DOCUMENT,
  documentName,
});
