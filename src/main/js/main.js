import 'babel-polyfill';
import { render } from 'react-dom';
import React from 'react';
import { AppContainer } from 'react-hot-loader';
import 'init';

import App from './App';

const renderComponent = (Component) => {
  render(
    <AppContainer>
      <Component/>
    </AppContainer>,
    document.getElementById('app'),
  );
};
renderComponent(App);

if (module.hot) {
  module.hot.accept('./App', () => {
    renderComponent(App);
  });
}
