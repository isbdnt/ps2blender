

export default async port => new Promise((resolve, reject) => {
  const ws = new WebSocket(`ws://localhost:${port}/`);
  ws.onopen = () => {
    debug('connected');
    resolve(ws);
  };
  ws.onerror = () => {
    reject(new Error('Blender plugin of Ps2Blender is not enabled'));
  };
});
