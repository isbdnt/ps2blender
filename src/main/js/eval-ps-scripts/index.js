import './eventBridge';
import saveActiveLayer from './saveActiveLayer';
import copy from './copy';
import test from './test';
import activeLayer from './activeLayer';
import activeDocument from './activeDocument';


export default window.__adobe_cep__;

export {
  saveActiveLayer, copy, test, activeLayer, activeDocument,
};
