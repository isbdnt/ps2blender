declare interface PsApp {
    activeDocument: PsDocument;
    documents: PsDocument[];
    preferences: PsPreferences;
    notifiersEnabled: boolean;
    notifiers: PsNotifier
}
declare interface PsNotifier {
    add(event: string, filePath: string): void;
}
declare interface PsDocument {
    activeLayer: PsArtLayer;
    artLayers: PsArtLayer[];
    saveAs(file: PsFile): void;
    saveAs(file: PsFile, options: PsPNGSaveOptions, copy: boolean, extCase: PsExtension): void;
    name: string;
}
declare interface PsArtLayer {
    visible: boolean;
}

declare interface PsPreferences {
    rulerUnits: string;
}
declare interface PsFile {

}
declare class PsPNGSaveOptions {

}
declare interface PsExtension {
    LOWERCASE: string;
    NONE: string;
    UPPERCASE: string;
}

declare interface PsUnits {
    INCHES: string;
}

declare const app: PsApp;
declare const Units: PsUnits;
declare function PNGSaveOptions(): PsPNGSaveOptions;
declare const Extension: PsExtension;
declare function evalPs(script: Function, ...params): Promise;