app.notifiersEnabled = true;
const removeOldNotifier = (name) => {
  const notifiers = Array.prototype.slice.call(app.notifiers);
  for (let i = 0; i < notifiers.length; i++) {
    const no = notifiers[i];
    if (no.event === name) no.remove();
  }
};

function onDocumentSave() {
  const file = new File('C:/Users/isbdnt/Documents/ps2blender/src/main/js/ps-scripts/onDocumentSave.jsx');
  app.notifiers.add('save', file, 'saveSucceeded');
}

function onDocumentOpen() {
  const file = new File('C:/Users/isbdnt/Documents/ps2blender/src/main/js/ps-scripts/onDocumentOpen.jsx');
  app.notifiers.add('Opn ', file);
}

function onDocumentSelect() {
  const file = new File('C:/Users/isbdnt/Documents/ps2blender/src/main/js/ps-scripts/onDocumentSelect.jsx');
  app.notifiers.add('slct', file);
}

function onDocumentSetData() {
  const file = new File('C:/Users/isbdnt/Documents/ps2blender/src/main/js/ps-scripts/onDocumentSetData.jsx');
  app.notifiers.add('setd', file);
}


(async () => {
  await evalPs(removeOldNotifier, '"save"');
  await evalPs(onDocumentSave);
  await evalPs(removeOldNotifier, '"Opn "');
  await evalPs(onDocumentOpen);
  await evalPs(removeOldNotifier, '"slct"');
  await evalPs(onDocumentSelect);
  await evalPs(removeOldNotifier, '"setd"');
  await evalPs(onDocumentSetData);
})();

