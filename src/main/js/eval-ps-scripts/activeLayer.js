function getField(field) {
  return app.activeDocument.activeLayer[field];
}

export default {
  async get(field) {
    return evalPs(getField, `"${field}"`);
  },
};
