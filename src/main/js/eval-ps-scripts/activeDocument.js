function getField(field) {
  return app.activeDocument[field];
}

export default {
  async get(field) {
    return evalPs(getField, `"${field}"`);
  },
};
