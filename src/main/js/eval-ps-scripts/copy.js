function copy() {
  app.activeDocument.activeLayer.copy();
}

export default () => evalPs(copy);
