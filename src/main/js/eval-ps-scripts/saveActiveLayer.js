function saveActiveLayer() {
  const originalUnit = app.preferences.rulerUnits;
  app.preferences.rulerUnits = Units.INCHES;
  const activeDoc = app.activeDocument;
  const activeLayer = activeDoc.activeLayer;
  const visibleTable = {};
  for (let i = 0; i < activeDoc.artLayers.length; i++) {
    const layer = activeDoc.artLayers[i];
    visibleTable[layer.name] = layer.visible;
    layer.visible = layer.name === activeLayer.name;
  }
  const file = new File(`C:/Program Files/Adobe/Adobe Photoshop CC 2017/Required/CEP/extensions/org.isbdnt.ps2blender/temp_img/${activeDoc.name}_${activeLayer.name}.png`);
  const opt = new PNGSaveOptions();
  opt.compression = 0;
  activeDoc.saveAs(file, opt, true, Extension.LOWERCASE);
  for (let i = 0; i < activeDoc.artLayers.length; i++) {
    const layer = activeDoc.artLayers[i];
    layer.visible = visibleTable[layer.name];
  }
  app.preferences.rulerUnits = originalUnit;
  return `${activeLayer.id},${activeLayer.name}`;
}

export default async () => {
  const result = (await evalPs(saveActiveLayer)).split(',');
  return {
    id: result[0],
    name: result[1],
  };
};
