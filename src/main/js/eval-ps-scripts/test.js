// const originalUnit = preferences.rulerUnits;
// preferences.rulerUnits = Units.INCHES;
// // Create a new 2x4 inch document and assign it to a variable
// let docRef = app.documents.add(2, 4);
// // Create a new art layer containing text
// let artLayerRef = docRef.artLayers.add();
// artLayerRef.kind = LayerKind.TEXT;
// // Set the contents of the text layer.
// let textItemRef = artLayerRef.textItem;
// textItemRef.contents = 'Hello, World';
// // Release references
// docRef = null;
// artLayerRef = null;
// textItemRef = null;
// // Restore original ruler unit setting
// app.preferences.rulerUnits = originalUnit;
// const artLayerRef = app.activeDocumentName.artLayers.add();
//
// artLayerRef.kind = LayerKind.NORMAL;

function test() {
  const a = [];
  for (const i in app.activeDocument.activeLayer) {
    a.push(i);
  }
  return app.activeDocument.activeLayer.id;
}

export default () => evalPs(test);
