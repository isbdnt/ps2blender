import { setConnectingBlender, setAllImageLinking } from 'actions';
import { dispatch, getState } from 'store';
import { TEMP_PATH, PORT } from 'config';
import { path } from 'nodejs';
import connectBlender from 'bridge-socket';


export default class BlenderAgent {
  static instance = null;

  static async connect() {
    const ws = await connectBlender(PORT);
    BlenderAgent.instance = new BlenderAgent(ws);
    dispatch(setConnectingBlender(true));
  }

  static disconnect() {
    BlenderAgent.instance = null;
    debug('disconnected');
    dispatch(setConnectingBlender(false));
    dispatch(setAllImageLinking(getState().activeDocumentName, false));
  }

  constructor(socket) {
    this._socket = socket;
    socket.onerror = BlenderAgent.disconnect;
    socket.onclose = BlenderAgent.disconnect;
  }

  linkImage(name) {
    debug('link image');
    this._imageMsg('link_image', name);
  }

  updateImage(name) {
    debug('update image');
    this._imageMsg('update_image', name);
  }

  // eslint-disable-next-line no-unused-vars,class-methods-use-this
  removeImage(name) {
    debug('remove image');
    // this._imageMsg('remove_image', name);
  }

  _imageMsg(type, imageName) {
    const doc = getState().activeDocumentName;
    const name = `${doc}_${imageName}.png`;
    this._socket.send(JSON.stringify({
      type,
      name,
      path: path.resolve(TEMP_PATH, name),
    }));
  }
}
