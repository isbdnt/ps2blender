import {
  ADD_IMAGE, SET_IMAGE_LINKING, REMOVE_IMAGE,
  SET_IMAGE_NAME, SET_ALL_IMAGE_LINKING,
} from 'action-types';


export default (state = [], action) => {
  switch (action.type) {
    case ADD_IMAGE:
      return [
        ...state,
        {
          id: action.imageId,
          name: action.imageName,
          isLinking: false,
        },
      ];
    case SET_IMAGE_LINKING:
      return state.map((img) => {
        if (img.id === action.imageId) {
          return {
            ...img,
            isLinking: action.isLinking,
          };
        }
        return img;
      });
    case REMOVE_IMAGE:
      return state.filter(img => img.id !== action.imageId);
    case SET_IMAGE_NAME:
      return state.map((img) => {
        if (img.id === action.imageId) {
          return {
            ...img,
            name: action.imageName,
          };
        }
        return img;
      });
    case SET_ALL_IMAGE_LINKING:
      return state.map(img => ({
        ...img,
        isLinking: action.isLinking,
      }));
    default :
      return state;
  }
};
