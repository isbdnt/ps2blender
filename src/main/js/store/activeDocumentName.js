import { SET_ACTIVE_DOCUMENT } from 'action-types';


export default (state = '', action) => {
  switch (action.type) {
    case SET_ACTIVE_DOCUMENT:
      return action.documentName;
    default:
      return state;
  }
};
