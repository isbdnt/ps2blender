import { ADD_DOCUMENT } from 'action-types';
import images from './images';

export default (state = {}, action) => {
  switch (action.type) {
    case ADD_DOCUMENT:
      return {
        ...state,
        [action.documentName]: [],
      };
    default:
      return {
        ...state,
        [action.documentName]: images(state[action.documentName], action),
      };
  }
};
