import { createStore } from 'redux';
// import logger from 'redux-logger';
import reducers from './reducers';

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

if (module.hot) {
  module.hot.accept('./reducers', () => {
    store.replaceReducer(
      reducers,
      window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    );
  });
}
export const dispatch = store.dispatch;
export const getState = store.getState;
export default store;

