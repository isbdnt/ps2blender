import { ADD_STDOUT_MESSAGE } from 'action-types';

export default (state = [], action) => {
  switch (action.type) {
    case ADD_STDOUT_MESSAGE:
      return [...state, action.message];
    default:
      return state;
  }
};
