import { combineReducers } from 'redux';
import stdoutMessages from './stdoutMessages';
import documents from './documents';
import activeDocumentName from './activeDocumentName';
import connectingBlender from './connectingBlender';

export default combineReducers({
  stdoutMessages,
  documents,
  activeDocumentName,
  connectingBlender,
});
