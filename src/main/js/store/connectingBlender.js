import { SET_CONNECTING_BLENDER } from 'action-types';


export default (state = false, action) => {
  switch (action.type) {
    case SET_CONNECTING_BLENDER:
      return action.isConnecting;
    default:
      return state;
  }
};
