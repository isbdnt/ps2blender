import BlenderAgent from 'BlenderAgent';


export default async () => {
  if (!BlenderAgent.instance) {
    try {
      await BlenderAgent.connect();
    }
      // eslint-disable-next-line no-empty
    catch (e) {
    }
  }
};
