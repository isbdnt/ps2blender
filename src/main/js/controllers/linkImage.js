import { setImageLinking, addImage } from 'actions';
import BlenderAgent from 'BlenderAgent';
import { saveActiveLayer } from 'eval-ps-scripts';
import { getState, dispatch } from 'store';

export default async () => {
  const { name, id } = await saveActiveLayer();
  const state = getState();
  const result = state.documents[state.activeDocumentName]
    .findIndex(img => img.id === id);
  if (result === -1) {
    dispatch(addImage(state.activeDocumentName, id, name));
  }
  dispatch(setImageLinking(state.activeDocumentName, id, state.connectingBlender));
  if (BlenderAgent.instance) {
    BlenderAgent.instance.linkImage(name);
  }
};
