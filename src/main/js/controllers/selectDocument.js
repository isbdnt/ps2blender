import { setActiveDocumentName } from 'actions';
import { dispatch, getState } from 'store';
import { activeDocument } from 'eval-ps-scripts';
import openDocument from './openDocument';


export default async () => {
  const name = await activeDocument.get('name');
  if (getState().documents[name]) {
    dispatch(setActiveDocumentName(name));
  }
  else {
    openDocument();
  }
};
