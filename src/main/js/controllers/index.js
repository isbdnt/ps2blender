import openDocument from './openDocument';
import linkImage from './linkImage';
import connectBlender from './connectBlender';
import updateImage from './updateImage';
import clickImage from './clickImage';
import removeImage from './removeImage';
import selectDocument from './selectDocument';
import checkImageName from './checkImageName';

export {
  openDocument, linkImage, connectBlender, updateImage, clickImage, removeImage,
  selectDocument, checkImageName,
};
