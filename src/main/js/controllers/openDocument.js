import { addDocument, setActiveDocumentName } from 'actions';
import { activeDocument } from 'eval-ps-scripts';
import { dispatch } from 'store';

export default async () => {
  const documentName = await activeDocument.get('name');
  dispatch(addDocument(documentName));
  dispatch(setActiveDocumentName(documentName));
};
