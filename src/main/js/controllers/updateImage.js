import { saveActiveLayer, activeLayer } from 'eval-ps-scripts';
import BlenderAgent from 'BlenderAgent';
import { getState } from 'store';

export default async () => {
  const id = await activeLayer.get('id');
  const state = getState();
  const image = state.documents[state.activeDocumentName].find(img => img.id === id);
  if (image.isLinking && BlenderAgent.instance) {
    const { name } = await saveActiveLayer();
    BlenderAgent.instance.updateImage(name);
  }
};
