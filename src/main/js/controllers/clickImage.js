import { setImageLinking } from 'actions';
import { dispatch, getState } from 'store';
import BlenderAgent from 'BlenderAgent';
import checkImageName from './checkImageName';


export default (id, isLinking) => {
  if (BlenderAgent.instance) {
    dispatch(setImageLinking(getState().activeDocumentName, id, !isLinking));
  }
  checkImageName();
};
