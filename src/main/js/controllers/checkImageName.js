import { activeLayer, saveActiveLayer } from 'eval-ps-scripts';
import { getState, dispatch } from 'store';
import { setImageName } from 'actions';
import BlenderAgent from 'BlenderAgent';

export default async () => {
  const state = getState();
  const id = await activeLayer.get('id');
  const name = await activeLayer.get('name');
  const image = state.documents[state.activeDocumentName].find(img => img.id === id);
  if (image && image.name !== name) {
    dispatch(setImageName(state.activeDocumentName, id, name));
    await saveActiveLayer();
    if (BlenderAgent.instance) {
      BlenderAgent.instance.removeImage(image.name);
      BlenderAgent.instance.linkImage(name);
    }
  }
};
