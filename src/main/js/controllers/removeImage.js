import { removeImage } from 'actions';
import { getState, dispatch } from 'store';
import BlenderAgent from 'BlenderAgent';

export default (id, name) => {
  dispatch(removeImage(getState().activeDocumentName, id));
  if (BlenderAgent.instance) {
    BlenderAgent.instance.removeImage(name);
  }
};
