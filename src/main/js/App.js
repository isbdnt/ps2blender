import React, { Component } from 'react';
import ps from 'eval-ps-scripts';
import store from 'store';
import { Provider } from 'react-redux';
import { Console, Images, ToolBar } from 'containers';
import { openDocument, updateImage, selectDocument, checkImageName } from 'controllers';

ps.addEventListener('onDocumentOpen', openDocument);
ps.addEventListener('onDocumentSave', updateImage);
ps.addEventListener('onDocumentSelect', selectDocument);
ps.addEventListener('onDocumentSetData', checkImageName);

openDocument();

export default class App extends Component {
  render = () => (
    <Provider store={store}>
      <div>
        <Console/>
        <ToolBar/>
        <Images/>
      </div>
    </Provider>
  );
}
