import asyncio
import websockets
import json
import bpy
from utils import debug

dirty = False


def link_image(msg):
    global dirty
    debug('link image')
    img = bpy.data.images.load(msg['path'], check_existing=True)
    # bpy.data.textures[msg['name']] = img
    img.reload()
    dirty = True


def update_image(msg):
    global dirty
    debug('update image')
    bpy.data.images[msg['name']].reload()
    dirty = True


def remove_image(msg):
    global dirty
    debug('remove image')


handle_message = {
    'link_image': link_image,
    'update_image': update_image,
    'remove_image': remove_image,
}


def check():
    return {"type": "test"}


async def consumer_handler(websocket):
    try:
        while True:
            msg_str = await websocket.recv()
            msg = json.loads(msg_str)
            handle_message[msg['type']](msg)
    except:
        return


async def producer_handler(websocket):
    try:
        while True:
            # msg = check()
            # await websocket.send(json.dumps(msg))
            await asyncio.sleep(1)
    except:
        return


async def handler(websocket, path):
    debug("ps connected")
    consumer_task = asyncio.ensure_future(consumer_handler(websocket))
    producer_task = asyncio.ensure_future(producer_handler(websocket))
    done, pending = await asyncio.wait(
        [consumer_task, producer_task],
        return_when=asyncio.FIRST_EXCEPTION,
    )
    for task in pending:
        task.cancel()
