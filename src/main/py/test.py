bl_info = {
    "name": "test",
    "category": "Object",
}

import sys
import bpy
from bpy.types import Panel, Operator, Scene
import asyncio
import threading

DEPENDENCIES_PATH = 'C:/Users/isbdnt/Documents/ps2blender/src/main/py/'
sys.path.append(DEPENDENCIES_PATH)

from utils import debug
import server
import websockets

sys.path.append(DEPENDENCIES_PATH)


class Ps2Blender(Panel):
    bl_label = "Ps2blender"
    bl_space_type = 'IMAGE_EDITOR'
    bl_region_type = 'UI'
    bl_context = "object"

    @classmethod
    def pool(self, context):
        return True

    def draw(self, context):
        col = self.layout.column(align=True)

        # row = col.row(align=True)
        # row.prop(context.scene, "blenderextpaint_export_uv")
        # row.prop(context.scene, "blenderextpaint_place")

        # col.operator("object.test", text="go")

        # row = col.row(align=True)
        col.prop(context.scene, "ps2blender_enable")
        col.operator("image.ps2blender_enable", "Enable")

        # row = col.row(align=True)
        # row.prop(context.scene, "blenderextpaint_autorefresh_pause_external")
        # row.prop(context.scene, "blenderextpaint_autorefresh_pause_blender")


class IMAGE_OT_ps2blender_enable(Operator):
    bl_idname = "image.ps2blender_enable"
    bl_label = "Minimal Operator"
    bl_description = "Autorefresh status"

    @classmethod
    def poll(self, context):
        return True

    def invoke(self, context, event):
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}

    def modal(self, context, event):
        if server.dirty:
            server.dirty = False
            for area in context.screen.areas:
                area.tag_redraw()

        self.report({'INFO'}, "Image reload")
        return {'PASS_THROUGH'}


class ServerThread(threading.Thread):
    _stopped = False

    def __init__(self, loop, port):
        super(ServerThread, self).__init__()
        self._loop = loop
        self._port = port
        self._running = threading.Event()
        self._running.set()

    def run(self):
        asyncio.set_event_loop(self._loop)
        self._loop.run_until_complete(websockets.serve(server.handler, 'localhost', self._port))
        self._loop.run_until_complete(self.check())

    def pause(self):
        self._running.clear()

    def resume(self):
        self._running.set()

    async def check(self):
        while True:
            self._running.wait()
            await asyncio.sleep(0.05)


server_thread = None


def enable(self, context):
    global server_thread
    if context.scene.ps2blender_enable:
        if server_thread is None:
            server_thread = ServerThread(asyncio.get_event_loop(), 8000)
            server_thread.start()
        else:
            server_thread.resume()
    else:
        if not server_thread is None:
            server_thread.pause()


def test(self, context):
    bpy.ops.image.reload()


def register():
    bpy.types.Scene.ps2blender_enable = \
        bpy.props.BoolProperty(name="Enable", description="Enable", default=False, update=enable)

    bpy.types.Scene.ps2blender_test = \
        bpy.props.BoolProperty(name="test", description="test", default=False, update=test)

    bpy.utils.register_class(IMAGE_OT_ps2blender_enable)
    bpy.utils.register_class(Ps2Blender)


def unregister():
    del bpy.types.Scene.ps2blender_enable
    del bpy.types.Scene.ps2blender_test

    bpy.utils.unregister_class(IMAGE_OT_ps2blender_enable)
    bpy.utils.unregister_class(Ps2Blender)
