import CopyPlugin from 'copy-webpack-plugin';
import path from 'path';

export default {
  // target:'electron',
  context: path.join(__dirname, 'src/main'),
  resolve: {
    modules: ['src/main/js', 'src/main/resources', 'node_modules'],
  },
  externals: {
    // fs: 'require("fs")',
    // path: 'require("path")',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      {
        test: /\.(png|jpg)$/,
        exclude: /node_modules/,
        use: 'url-loader?limit=10000',
      },
      {
        test: /\.(ttf|woff|woff2|eot|svg)$/,
        exclude: /node_modules/,
        use: 'file-loader',
      },
    ],
  },
  plugins: [
    new CopyPlugin([
      { from: './resources/img/logo.png', to: 'logo.png' },
      { from: './resources/config', to: './' },
      { from: './js/ps-scripts', to: 'jsx' },
    ]),
  ],
};
